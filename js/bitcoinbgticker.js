window.onload = function () {
    (function () {
        var settings = {
            currency: {
                interval: 3600 * 1000, // per hour 
                url: 'proxy/usdbgn.json',
            },
            exchange: {
                bitfinex: {
                    disabled: false,
                    name: 'Bitfinex',
                    request: {
                        channel: 'ticker',
                        event: 'subscribe',
                        pair: 'BTCUSD',
                    },
                    url: 'wss://api-pub.bitfinex.com/ws/2',
                },
                bitstamp: {
                    disabled: false,
                    name: 'Bitstamp',
                    interval: 10, // min 10 seconds
                    request: {
                        "event": "bts:subscribe",
                        "data": {
                            "channel": "live_trades_btcusd"
                        }
                    },
                    url: 'wss://ws.bitstamp.net',
                },
                coinbase: {
                    disabled: false,
                    name: 'Coinbase',
                    interval: 10000, // 10 seconds
                    start_after: 0100, // 0.1 seconds
                    url: 'https://api.coinbase.com/v2/prices/BTC-USD/spot',
                },
                kraken: {
                    disabled: false,
                    name: 'Kraken',
                    interval: 10000, // 10 seconds
                    start_after: 0100, // 0.1 seconds
                    url: 'proxy/kraken.json',
                },
                binance: {
                    disabled: false,
                    name: 'Binance',
                    interval: 10000, // 10 seconds
                    start_after: 0100, // 0.1 seconds
                    url: 'https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT',
                },
                bitmex: {
                    disabled: true,
                    name: 'BitMEX',
                    interval: 10000, // 10 seconds
                    request: {
                        args: 'trade:XBTUSD',
                        op: 'subscribe',
                    },
                    url: 'wss://www.bitmex.com/realtime',
                },
            },
            events: {
                bitfinex: 'price_update_bitfinex',
                bitstamp: 'price_update_bitstamp',
                coinbase: 'price_update_coinbase',
                kraken: 'price_update_kraken',
                binance: 'price_update_binance',
                bitmex: 'price_update_bitmex',
            },
            html: {
                placeholder: {
                    odoticker: document.getElementById('odoticker'),
                    bgnticker: document.getElementById('bgnticker'),
                },
            },
            odometer: {
                auto: false,
                format: '( , ddd).dd',
                duration: 2048,
            },
        };

        new BitcoinBGTicker(settings).init();


        function BitcoinBGTicker(settings) {
            var self = this;

            self.currency = {};
            self.price = 0;
            self.prices = {};
            self.settings = settings;

            self.init = function () {
                browsersPolyfills();
                createPriceEvents();
				getCurrencyExchangeRate();
                initExchanges();
            };

            self.emitPriceEvent = function (event) {
                document.dispatchEvent(event);
            };

            function initExchanges() {
                Object.keys(self.settings.exchange).forEach(function (e) {
                    var exchange = self.settings.exchange[e],
                        init_function = 'init' + exchange.name;

                    if (exchange.disabled) return;

                    self[init_function]();
                });
            }

            function createPriceEvents() {
                self.events = {};

                Object.keys(self.settings.events).forEach(function (e) {
                    self.events[e] = new CustomEvent(e, {
                        detail: self.settings.events[e],
                    });
                })

                Object.keys(self.events).forEach(function (e) {
                    self.prices[self.events[e].type] = null;
                });

                self.handlePriceEvent();
            }

            self.handlePriceEvent = function () {
                Object.keys(self.events).forEach(function (e) {
                    var event = self.events[e];

                    document.addEventListener(event.type, handlePriceEvent);
                });

                function handlePriceEvent(event) {
                    var price = 0,
                        exchanges = 0,
                        event_type = event.type,
                        requirements = true;

                    requirements &= !isNaN(self.price);
                    requirements &= self.prices[event_type] !== self.price;

                    if (!requirements) return;

                    self.prices[event_type] = self.price;

                    console.log('event_type:', event_type, self.prices); //remove in prod

                    Object.keys(self.prices).forEach(function (e) {
                        if (self.prices[e]) {
                            price += self.prices[e];
                            exchanges++;
                        }
                    });

                    self.price = (price / exchanges).toFixed(2);

                    if (handlePriceEvent.timeout) {
                        clearTimeout(handlePriceEvent.timeout);
                    }

                    handlePriceEvent.timeout = setTimeout(function () {
                        self.settings.html.placeholder.odoticker.innerHTML = self.price;
                        self.settings.html.placeholder.bgnticker.innerHTML =
                            (self.price * self.currency.usdbgn).toFixed(2);
                    });
                }
            };

            self.initBitfinex = function () {
                var event = self.events.bitfinex,
                    settings = self.settings.exchange.bitfinex,
                    ws = new WebSocket(settings.url);

                ws.onopen = function () {
                    ws.send(JSON.stringify(settings.request));
                };

                ws.onmessage = function (response) {
                    var data = JSON.parse(response.data);
                    
                    initPrice(data);
                };
				
				function initPrice(data) {
					if ((data.event == null) & (data[1] != 'hb')){
						_initPrice_(event, data[1][6]);
					}	
                }
            }

            self.initBitstamp = function () {
                var event = self.events.bitstamp,
                    settings = self.settings.exchange.bitstamp,
                    ws = new WebSocket(settings.url);
                    lastupdate = 0;

                    ws.onopen = function () {
                        ws.send(JSON.stringify(settings.request));
                    };
                    
                    ws.onmessage = function (response) {
                        var data = JSON.parse(response.data);
                        switch (data.event) {
                            case 'trade': {
                                initPrice(data.data);
                                break;
                            }
                            case 'bts:request_reconnect': {
                                self.initBitstamp();
                                break;
                            }
                        }
                    };

                    function initPrice(data) {
                        if (lastupdate <= (data.timestamp - settings.interval)) {
                            _initPrice_(event, data.price);
                            lastupdate = data.timestamp;
                        }
                    }
            }

            self.initCoinbase = function () {
                var event = self.events.coinbase,
                    settings = self.settings.exchange.coinbase;

                setTimeout(startPolling, settings.start_after);
                setInterval(startPolling, settings.interval);

                function startPolling() {
                    ajax(settings.url, function (response) {
                        var data = JSON.parse(response);

                        _initPrice_(event, data.data.amount);
                    });
                }
            }

            self.initKraken = function () {
                var event = self.events.kraken,
                    settings = self.settings.exchange.kraken;

                setTimeout(startPolling, settings.start_after);
                setInterval(startPolling, settings.interval);

                function startPolling() {
                    ajax(settings.url, function (response) {
                        var data = JSON.parse(response);
						var BTCEURtoUSD = data.rates.XBTEUR * self.currency.eurusd;
                        _initPrice_(event, ((BTCEURtoUSD + data.rates.XBTUSD) / 2).toFixed(2));
                    });
                }
            }

            self.initBinance = function () {
                var event = self.events.binance,
                    settings = self.settings.exchange.binance;

                setTimeout(startPolling, settings.start_after);
                setInterval(startPolling, settings.interval);

                function startPolling() {
                    ajax(settings.url, function (response) {
                        var data = JSON.parse(response);
                        _initPrice_(event, data.price);
                    });
                }
            }

            self.initBitMEX = function () {
                var event = self.events.bitmex,
                    settings = self.settings.exchange.bitmex,
                    ws = new WebSocket(settings.url),
                    lastupdate = 0;
                
                ws.onopen = function () {
                    ws.send(JSON.stringify(settings.request));
                };
                
                ws.onmessage = function (response) {
                    var data = JSON.parse(response.data).data;

                    if (data[0].size >= 500){
                        initPrice(data);
                    }
                };
                
                function initPrice(data) {
                    if (lastupdate < (Date.now() - settings.interval)) {
                        _initPrice_(event, data[0].price);
                        lastupdate = Date.now();
                    }
                }
            }

            function _initPrice_(event, price) {
                self.price = +price;
                self.emitPriceEvent(event);
            }

            function getCurrencyExchangeRate() {

                startPolling();

                setInterval(startPolling, self.settings.currency.interval);

                function startPolling() {
                    ajax(self.settings.currency.url, function (response) {
                        var data = JSON.parse(response).rates;

                        Object.keys(data).forEach(function (e) {
                            self.currency[e.toLowerCase()] = data[e];

                        });
                    });
                }
            }

            function ajax(url, callback) {
                var xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        callback(xmlhttp.responseText);
                    }
                }

                xmlhttp.open('GET', url, true);
                // xmlhttp.setRequestHeader('Cache-Control', 'max-age=0');

                xmlhttp.send();

            }
        }

        // IE miseries
        function browsersPolyfills() {
            polyfillEventIE();

            function polyfillEventIE() {
                if (typeof window.CustomEvent === 'function') return false;

                function CustomEvent(event, params) {
                    params = params || {
                        bubbles: false,
                        cancelable: false,
                        detail: undefined,
                    };

                    var evt = document.createEvent('CustomEvent');

                    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);

                    return evt;
                }

                CustomEvent.prototype = window.Event.prototype;

                window.CustomEvent = CustomEvent;
            }
        }
    })();
};
